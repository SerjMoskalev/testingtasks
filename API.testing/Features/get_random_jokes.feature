Feature: Changing the name of the main character. The API permits changing the name of the main character when fetching a joke
	Scenario Outline: Get some random jokes
		Given Chuck Norris API is working
		When Get a joke with custom "<firstname>" and "<lastname>"
		Then The response code is 200
		And The API returns a random jokes with my "<firstname>" and "<lastname>"

	Examples: Name
   	| firstname	| lastname 	|
   	| Sergey	| Moskalev 	|
   	| John 		| Smith		|