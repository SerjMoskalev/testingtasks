Feature: Fetching multiple random jokes
	Scenario: Get some random jokes
		Given Chuck Norris API is working
		When I sent GET request to fetch 3 jokes
		Then The response code is 200
		And The API returns 3 random jokes
