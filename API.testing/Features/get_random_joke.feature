Feature: Fetching a random joke
	Scenario: Get a random joke
		Given Chuck Norris API is working
		When I sent GET request to fetch a joke
		Then The response code is 200
		And The API returns a random joke
