import requests


@when(u'I sent GET request to fetch 3 jokes')
def step_impl(context):
    context.get_response = requests.get(context.api_url + '/jokes/random/3')

@then(u'The API returns 3 random jokes')
def step_impl(context):
    assert len(context.get_response.json()['value']) == 3