import requests


@when(u'Get a joke with custom "{firstname}" and "{lastname}"')
def step_impl(context, firstname, lastname):
    context.get_response = requests.get(context.api_url + '/jokes/random?firstName='+firstname+'&lastName='+lastname)

@then(u'The API returns a random jokes with my "{firstname}" and "{lastname}"')
def step_impl(context, firstname, lastname):
    assert firstname in context.get_response.json()['value']['joke']
    assert lastname in context.get_response.json()['value']['joke']
