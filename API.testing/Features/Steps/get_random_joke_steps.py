import requests


@given(u'Chuck Norris API is working')
def step_impl(context):
    context.api_url = 'http://api.icndb.com'

@when(u'I sent GET request to fetch a joke')
def step_impl(context):
    context.get_response = requests.get(context.api_url + '/jokes/random')

@then(u'The response code is 200')
def step_impl(context):
    assert context.get_response.status_code == 200

@then(u'The API returns a random joke')
def step_impl(context):
    assert context.get_response.json()['value']['joke']