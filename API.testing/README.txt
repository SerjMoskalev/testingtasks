This is a automation test for free public API that returns Chuck Norris, the famous actor, jokes.

API documentation site:
http://www.icndb.com/api/

The test documentation:
Even though behaveoir driven testing has all needed documents in itself, the test cases are also written in TestCases.docx file.

Test automation tools:
The tests will be written in BDD style using Behave + Python.

Behave fucntions documentation:
https://behave.readthedocs.io/en/latest/

To run tests:
Open windows terminal in the Features forlder and run "behave" command.